from django.contrib import admin
from .models import Matkul, Tugas

admin.site.register(Matkul)
admin.site.register(Tugas)