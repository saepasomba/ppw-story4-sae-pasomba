# Generated by Django 3.0.3 on 2020-02-26 04:04

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='matkul',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('dosen', models.CharField(max_length=50)),
                ('sks', models.IntegerField(max_length=2)),
                ('deskripsi', models.CharField(max_length=150)),
                ('semester', models.CharField(max_length=20)),
                ('kelas', models.CharField(max_length=50)),
            ],
        ),
    ]
