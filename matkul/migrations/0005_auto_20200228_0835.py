# Generated by Django 3.0.3 on 2020-02-28 08:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('matkul', '0004_auto_20200228_0833'),
    ]

    operations = [
        migrations.AddField(
            model_name='tugas',
            name='dueToDL',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='tugas',
            name='x',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='matkul.Matkul'),
        ),
    ]
