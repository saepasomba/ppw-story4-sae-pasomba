from django.shortcuts import render, redirect
from django.core.serializers import serialize
import json
from .models import Matkul, MatkulForm, Tugas

def formMatkul(request):   
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        
        if form.is_valid():
            form.save()

            return redirect('matkul:formMatkul')
        
    else:
        form = MatkulForm()


    return render(request, 'form.html', {'form': form})

def resultMatkul(request):

    data = Matkul.objects.all()
    x_data = json.loads(serialize('json', data))

    return render(request, 'result.html', {'data':x_data})

def detailsMatkul(request, pkTarget):
    details = Matkul.objects.filter(pk=pkTarget)
    detailsTarget = json.loads(serialize('json', details))

    tugasMatkul = Tugas.objects.all().order_by('dueToDL')

    return render(request, 'details.html', {'details':detailsTarget})

def deleteMatkul(request, pkTarget):
    target = Matkul.objects.filter(pk=pkTarget)
    target.delete()

    x_data = json.loads(serialize('json', Matkul.objects.all()))

    return redirect('matkul:resultMatkul')


