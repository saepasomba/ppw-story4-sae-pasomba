from django.db import models
from django.forms import ModelForm
from django.core.validators import MinValueValidator, MaxValueValidator

class Matkul(models.Model):
    nama = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.IntegerField(validators=[MinValueValidator(0)])
    deskripsi = models.CharField(max_length=1500)
    semester = models.CharField(max_length=20)
    kelas = models.CharField(max_length=50)

class MatkulForm(ModelForm):
    class Meta:
        model = Matkul
        fields = ['nama', 'dosen', 'sks', 'deskripsi', 'semester', 'kelas']

class Tugas(models.Model):
    x = models.ForeignKey(Matkul, on_delete=models.CASCADE)
    namaDeadline = models.CharField(max_length=50)
    matkul = models.CharField(max_length=50)
    dueToDL = models.IntegerField()