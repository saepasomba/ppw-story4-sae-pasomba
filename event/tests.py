from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Event, Member
from .views import eventIndex, getEvent, getMember
from .forms import EventForm, MemberForm
from django.apps import apps
from .apps import EventConfig



# Create your tests here.
class EventTestCase(TestCase):

    def test_event_url_is_exist(self):
        response = Client().get('/event/')
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(EventConfig.name, 'event')
        self.assertEqual(apps.get_app_config('event').name, 'event')

    def test_eventIndex_from_views(self):
        found = resolve('/event/')
        self.assertEqual(found.func, eventIndex)

    def test_eventIndex_url_is_exist(self):
        response = Client().get('/event/')
        self.assertEqual(response.status_code, 200)
    
    def test_getEvent_from_views(self):
        found = resolve('/event/addEvent/')
        self.assertEqual(found.func, getEvent)

    def test_getEvent_url_is_exist(self):
        response = Client().get('/event/addEvent/')
        self.assertEqual(response.status_code, 200)

    def test_getMember_from_views(self):
        found = resolve('/event/addMember/2/')
        self.assertEqual(found.func, getMember)

    def test_getMember_url_is_exist(self):
        response = Client().get('/event/addMember/2/')
        self.assertEqual(response.status_code, 200)
    
class TestAddEvent(TestCase):
	def test_addEvent_url_is_exist(self):
		response = Client().get('/event/addEvent/')
		self.assertEqual(response.status_code, 200)

	def test_addEvent_using_template(self):
		response = Client().get('/event/addEvent/')
		self.assertTemplateUsed(response, 'addEvent.html')

	def test_Event_model_create_new_object(self):
		acara = Event(nama ="test")
		acara.save()
		self.assertEqual(Event.objects.all().count(), 1)

class TestModel(TestCase):
    def test_model_can_print(self):
        event = Event.objects.create(nama="dummy event", deskripsi="loremIpsum")
        self.assertEqual(event.__str__(), "dummy event")

class TestingAddMember(TestCase):
    def setUp(self):
        acara = Event(nama='santuy bersama')
        acara.save()

    def test_add_Member_url_is_exist(self):
        response = Client().post('/event/addMember/1', data={'nama':'loremIpsummm'})
        self.assertEqual(response.status_code, 301)

class TestingModels(TestCase):
    def test_Event_models_can_create_object(self):
        testObj = Event.objects.create(nama='Dummy', deskripsi='DummyLoremIpsum')
    
        countingAllEvent = Event.objects.all().count()
        self.assertEqual(countingAllEvent, 1)

    def test_Member_models_can_create_object(self):
        eventTmp = Event.objects.create(nama='Dummy', deskripsi='DummyLoremIpsum')
        testObj = Member.objects.create(nama='Dummy', event=Event.objects.all()[0])
        countingAllMember = Member.objects.all().count()
        self.assertEqual(countingAllMember,1)

class TestDeleteMember(TestCase):
	def test_delete_Member_url_is_exist(self):
		response = Client().get('event/1/')
		self.assertEqual(response.status_code, 404)
    
	def test_member_delete(self):
		event = Event.objects.create(nama="DummyLorem", deskripsi="KerenDummy")
		target = Member.objects.create(nama="Dummy lololo", event=event)
		target.delete()
		self.assertEqual(Member.objects.all().count(), 0)