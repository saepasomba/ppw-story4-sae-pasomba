from django.urls import path
from . import views

app_name = 'event'

urlpatterns = [
    path('', views.eventIndex, name='eventIndex'),
    path('addEvent/', views.getEvent, name='getEvent'),
    path('addMember/<int:pkTarget>/', views.getMember, name='getMember'),
    path('/<int:pkTarget>', views.deleteMember, name='deleteMember'),
]