from django import forms
from django.forms import ModelForm
from .models import Event, Member

class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = ['nama', 'deskripsi']

class MemberForm(ModelForm):
    class Meta:
        model = Member
        fields = ['nama']