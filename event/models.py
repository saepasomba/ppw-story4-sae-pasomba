from django.db import models
from django.forms import ModelForm

# Create your models here.
class Event(models.Model):
    nama = models.CharField('Nama event', max_length=50)
    deskripsi = models.TextField()

    def __str__(self):
        return self.nama

class Member(models.Model):
    nama = models.CharField('Nama', max_length=25)
    event = models.ForeignKey('Event', on_delete=models.CASCADE)

    def __str__(self):
        return self.nama