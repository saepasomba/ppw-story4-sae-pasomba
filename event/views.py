from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .models import Event, Member
from .forms import EventForm, MemberForm
from django import forms

# Create your views here.
def eventIndex(request):
    dataEvent = Event.objects.all()
    dataMember = Member.objects.all()

    return render(request, 'event.html', {'event':dataEvent, 'member':dataMember})

def getEvent(request):
    if request.method == 'POST':
        form = EventForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('event:eventIndex')
    
    else:
        form = EventForm()
    
    return render(request, 'addEvent.html', {'form':form})

def getMember(request, pkTarget=0):
    if request.method == 'POST':
        form = MemberForm(request.POST)
        if form.is_valid():
            form = Member(nama=form.data['nama'], event=Event.objects.filter(pk=pkTarget)[0])
            form.save()

            return redirect('event:eventIndex')
    
    else:
        form = MemberForm()
    
    return render(request, 'addMember.html', {'form':form})

def deleteMember(request, pkTarget):
    target = Member.objects.filter(pk=pkTarget)
    target.delete()

    dataMember = Member.objects.all()

    return redirect('event:eventIndex')
