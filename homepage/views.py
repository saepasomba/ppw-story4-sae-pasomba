from django.shortcuts import render
import datetime


def index(request):
    return render(request, 'index.html')

def skills(request):
    return render(request, 'skills.html')

def clock(request, code=0):
    return render(request, 'clock.html', context={'value':datetime.datetime.now() + datetime.timedelta(hours=code)})