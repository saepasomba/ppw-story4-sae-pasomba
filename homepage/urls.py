from django.urls import path, register_converter
from . import views, converters

app_name = 'homepage'

register_converter(converters.NegativeIntConverter, 'negint')

urlpatterns = [
    path('', views.index, name='index'),
    path('skills/', views.skills, name='skills'),
    path('#desc', views.index, name='index'),
    path('clock/<negint:code>/', views.clock, name='clock'),
    path('clock/', views.clock, name='clock'),

]